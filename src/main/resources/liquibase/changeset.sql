--liquibase formatted sql

--changeset raven:1
create table setting(
    mnemo varchar(32) not null,
    value varchar(32),
    primary key(mnemo)
);

/* user settings */
insert into setting(mnemo, value) values('LOCALE', null);
