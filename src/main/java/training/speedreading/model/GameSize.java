package training.speedreading.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Game size container.
 * @author Alexey Chalov
 */
@Getter
@EqualsAndHashCode
@RequiredArgsConstructor
public class GameSize {

    private final int width;
    private final int height;

    /**
     * Returns game size display string.
     * @return game size display string
     */
    public String displayString() {
        return String.format("%dx%d", width, height);
    }
}
