package training.speedreading.model;

/**
 * Speed dynamic behavior types enumeration.
 * @author Alexey Chalov
 */
public enum SpeedDynamics {

    FIXED, ADJUSTING;
}
