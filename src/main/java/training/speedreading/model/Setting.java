package training.speedreading.model;

/**
 * Settings enumeration.
 * @author Alexey Chalov
 */
public enum Setting {

    /* user settings */
    LOCALE;
}
