package training.speedreading.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Locale;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import training.speedreading.model.Setting;
import training.speedreading.util.GameUtil;

/**
 * H2 database manager.
 * @author Alexey Chalov
 */
final class H2DbManager {

    private static H2DbManager dbManager = initDb();
    private static Connection connection;

    private static final int DB_VERSION = 1;

    /**
     * Constructor.
     */
    private H2DbManager() {
    }

    /**
     * Returns {@link H2DbManager} instance.
     * @return {@link H2DbManager} instance
     */
    public static H2DbManager instance() {
        return dbManager;
    }

    /**
     * Saves user setting.
     * @param setting setting constant
     * @param value setting value
     */
    @SneakyThrows
    public void saveSetting(Setting setting, Object value) {
        Connection conn = getConnection();
        String val = null;
        if (value.getClass() == Locale.class) {
            val = ((Locale) value).getLanguage() + "_" + ((Locale) value).getCountry();
        } else if (value.getClass() == Integer.class) {
            val = String.valueOf(value);
        }
        String query =
              "update setting"
            + "   set value = ?"
            + " where mnemo = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, val);
            ps.setString(2, setting.name());
            ps.executeUpdate();
        }
    }

    /**
     * Returns user or system setting.
     * @param <T> return type
     * @param setting setting constant
     * @param targetClass class to cast result to
     * @return setting value
     */
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public <T> T getSetting(Setting setting, Class<T> targetClass) {
        Connection conn = getConnection();
        Object result = null;
        String query =
              "select value"
            + "  from setting"
            + " where mnemo = ?";
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setString(1, setting.name());
            ResultSet rs = ps.executeQuery();
            rs.next();
            String val = rs.getString(1);
            if (val != null && !val.trim().isEmpty()) {
                if (targetClass == Locale.class) {
                    String[] localeData = val.split("_");
                    result = new Locale(localeData[0], localeData[1]);
                } else if (targetClass == Integer.class) {
                    result = rs.getInt(1);
                }
            }
        }
        return (T) result;
    }

    /**
     * Initializes {@link H2DbManager} instance.
     * @return {@link H2DbManager} instance
     */
    @SneakyThrows
    private static H2DbManager initDb() {
        File dataDir = new File(GameUtil.DATA_DIR);
        if (!dataDir.exists()) {
            dataDir.mkdir();
        }
        boolean runLiquibase = true;
        File dbVersionFile = new File(GameUtil.DB_VERSION_FILE);
        if (dbVersionFile.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(dbVersionFile))) {
                int dbVersion = Integer.parseInt(br.readLine());
                runLiquibase = dbVersion != DB_VERSION;
            }
        }
        if (runLiquibase || !dbVersionFile.exists()) {
            new File(dbVersionFile.getParent()).mkdirs();
            try (FileWriter fw = new FileWriter(dbVersionFile)) {
                fw.write(String.valueOf(DB_VERSION));
            }
            liquibaseUpdate();
        }
        return new H2DbManager();
    }

    /**
     * Setups database structure.
     */
    @SneakyThrows
    private static void liquibaseUpdate() {
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(getConnection()));
        Liquibase liquibase = new Liquibase(
            "/liquibase/changeset.sql", new ClassLoaderResourceAccessor(), database
        );
        liquibase.clearCheckSums();
        liquibase.update((String) null);
        liquibase.close();
    }

    /**
     * Initializes database connection.
     * @return {@link Connection} object
     */
    @SneakyThrows
    private static Connection getConnection() {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:h2:file:" + GameUtil.DB_FILE + ";TRACE_LEVEL_FILE=0");
        }
        return connection;
    }
}
