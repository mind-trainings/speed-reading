package training.speedreading.manager;

import static training.speedreading.model.Setting.LOCALE;
import static training.speedreading.util.GameUtil.SUPPORTED_LOCALES;
import static training.speedreading.util.GameUtil.guessLocale;

import java.util.Locale;
import java.util.prefs.Preferences;

import lombok.SneakyThrows;
import training.speedreading.model.GameSize;
import training.speedreading.model.SpeedDynamics;

/**
 * User settings manager.
 * @author Alexey Chalov
 */
public final class SettingManager {

    private H2DbManager dbManager;
    private static SettingManager settingManager = new SettingManager();

    private static final Preferences SETTINGS = Preferences.userNodeForPackage(SettingManager.class);

    private static final String LATEST_PLAYED_GAME_SIZE_WIDTH = "gsizew";
    private static final String LATEST_PLAYED_GAME_SIZE_HEIGHT = "gsizeh";
    private static final String LATEST_PLAYED_GAME_SPEED_DYNAMICS = "gspeeddyn";
    private static final String LATEST_PLAYED_GAME_SPEED = "gspeed";

    public static final int MIN_GAME_SIZE = 3;
    public static final int MAX_GAME_SIZE = 6;

    private static final int DEFAULT_GAME_SPEED = 300;

    /**
     * Constructor.
     */
    private SettingManager() {
        dbManager = H2DbManager.instance();
    }

    /**
     * Returns instance of setting manager.
     * @return {@link SettingManager} instance
     */
    public static SettingManager instance() {
        return settingManager;
    }

    /**
     * Saves settings.
     * @param gameSize {@link GameSize} instances
     * @param dynamics {@link SpeedDynamics} constant
     * @param speed speed
     */
    public void saveSettings(GameSize gameSize, SpeedDynamics dynamics, int speed) {
        SETTINGS.put(LATEST_PLAYED_GAME_SIZE_WIDTH, String.valueOf(gameSize.getWidth()));
        SETTINGS.put(LATEST_PLAYED_GAME_SIZE_HEIGHT, String.valueOf(gameSize.getHeight()));
        SETTINGS.put(LATEST_PLAYED_GAME_SPEED_DYNAMICS, dynamics.name());
        SETTINGS.put(LATEST_PLAYED_GAME_SPEED, String.valueOf(speed));
        flushSettings();
    }

    /**
     * Returns latest played game speed.
     * @return latest played game speed
     */
    public int getSpeed() {
        return SETTINGS.getInt(LATEST_PLAYED_GAME_SPEED, DEFAULT_GAME_SPEED);
    }

    /**
     * Returns latest played game speed dynamics type.
     * @return {@link SpeedDynamics} constant
     */
    public SpeedDynamics getSpeedDynamics() {
        String dynamics = SETTINGS.get(LATEST_PLAYED_GAME_SPEED_DYNAMICS, null);
        if (dynamics != null) {
            try {
                return SpeedDynamics.valueOf(dynamics);
            } catch (Exception e) {
            }
        }
        return SpeedDynamics.FIXED;
    }

    /**
     * Returns latest played game size.
     * Note, score calculation uses only one of two measures, non equal values in width and height may cause problems.
     * @return latest played game size
     */
    public GameSize getGameSize() {
        return new GameSize(
            SETTINGS.getInt(LATEST_PLAYED_GAME_SIZE_WIDTH, MIN_GAME_SIZE),
            SETTINGS.getInt(LATEST_PLAYED_GAME_SIZE_HEIGHT, MIN_GAME_SIZE)
        );
    }

    /**
     * Returns user selected locale.
     * @return {@link Locale} instance
     */
    public Locale getLocale() {
        Locale locale = dbManager.getSetting(LOCALE, Locale.class);
        if (SUPPORTED_LOCALES.contains(locale)) {
            return locale;
        }
        return guessLocale();
    }

    /**
     * Saves locale.
     * @param locale {@link Locale} instance
     */
    public void saveLocale(Locale locale) {
        if (SUPPORTED_LOCALES.contains(locale)) {
            dbManager.saveSetting(LOCALE, locale);
        }
    }


    /**
     * Saves settings to persistent store.
     */
    @SneakyThrows
    private void flushSettings() {
        SETTINGS.flush();
    }
}
