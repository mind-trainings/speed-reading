package training.speedreading.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utility class.
 * @author Alexey Chalov
 */
public final class GameUtil {

    public static final Locale LOCALE_RU = new Locale("ru", "RU");
    public static final Locale LOCALE_UK = Locale.UK;
    public static final Set<Locale> SUPPORTED_LOCALES = Arrays.stream(new Locale[] {LOCALE_UK, LOCALE_RU})
                                                              .collect(Collectors.toSet());

    public static final String DATA_DIR =
            System.getProperty("user.home") + System.getProperty("file.separator")
          + "mind-trainings" + System.getProperty("file.separator") + "speed-reading" + System.getProperty("file.separator");
    public static final String DB_VERSION_FILE = DATA_DIR + "db.version";
    public static final String DB_FILE = DATA_DIR + "speed-reading";
    public static final String DB_FILE_FULL = DB_FILE + ".mv.db";

    private static final ClassLoader CLASS_LOADER = GameUtil.class.getClassLoader();

    private static Map<Locale, List<String>> wordMap;

    static {
        initialize();
    }

    /**
     * Constructor.
     */
    private GameUtil() {
    }

    /**
     * Tries to guess locale using default one.
     * @return {@link Locale} instance
     */
    public static Locale guessLocale() {
        return Locale.getDefault().getLanguage().equals(LOCALE_UK.getLanguage()) ? LOCALE_UK : LOCALE_RU;
    }

    /**
     * Returns new random word list for particular locale.
     * @param locale {@link Locale} instance
     * @param gameSize game size
     * @return random word list
     */
    public static List<String> newWordList(Locale locale, int gameSize) {
        int minWords = 6;
        /* two rounds */
        int maxWords = gameSize * 2;
        int listSize = minWords + new Random().nextInt(maxWords - minWords);
        List<String> words = wordMap.get(locale);
        Collections.shuffle(words);
        return new ArrayList<String>(words.subList(0, listSize));
    }

    /**
     * Formats input time in milliseconds to human readable value.
     * @param time time in milliseconds
     * @return formatted time string
     */
    public static String formatTime(Number time) {
        int seconds = time.intValue() / 1000 % 60;
        return time.intValue() / 60000 + ":" + (seconds < 10 ? "0" : "") + seconds;
    }

    /**
     * Returns URL for resource at path.
     * @param path resource path
     * @return resource {@link URL}
     */
    public static URL getClassPathResourceUrl(String path) {
        return CLASS_LOADER.getResource(path);
    }

    /**
     * Initializes dictionaries.
     */
    private static void initialize() {
        wordMap = new HashMap<>();
        SUPPORTED_LOCALES.forEach(l -> wordMap.put(l, readDictionary(l)));
    }

    /**
     * Reads dictionary into list of words and returns it.
     * @param locale {@link Locale} instance
     * @return list of words
     */
    private static List<String> readDictionary(Locale locale) {
        List<String> words = new ArrayList<>();
        try (InputStream is = CLASS_LOADER.getResourceAsStream("bundle/dictionary_" + locale.getLanguage() + "_" + locale.getCountry());
            BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            String word;
            while ((word = br.readLine()) != null) {
                words.add(word);
            }
        } catch (Exception e) {
        }
        return words;
    }
}
