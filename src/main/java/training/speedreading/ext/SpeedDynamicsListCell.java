package training.speedreading.ext;

import java.util.ResourceBundle;

import javafx.scene.control.ListCell;
import training.speedreading.DataBridge;
import training.speedreading.model.SpeedDynamics;

/**
 * {@link ListCell} implementation for speed dynamics behavior types.
 * @author Alexey Chalov
 */
public final class SpeedDynamicsListCell extends ListCell<SpeedDynamics> {

    private ResourceBundle bundle = DataBridge.instance().getResourceBundle();

    @Override
    protected void updateItem(SpeedDynamics item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setText(bundle.getString(item.name()));
        }
    }
}
