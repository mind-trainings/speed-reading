package training.speedreading.ext;

import javafx.scene.control.ListCell;
import training.speedreading.model.GameSize;

/**
 * {@link ListCell} implementation for game sizes.
 * @author Alexey Chalov
 */
public class GameSizeListCell extends ListCell<GameSize> {

    @Override
    protected void updateItem(GameSize gameSize, boolean empty) {
        super.updateItem(gameSize, empty);
        if (gameSize != null) {
            setText(gameSize.displayString());
        }
    }
}
