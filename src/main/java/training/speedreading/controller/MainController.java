package training.speedreading.controller;

import static javafx.collections.FXCollections.observableArrayList;
import static training.speedreading.util.GameUtil.getClassPathResourceUrl;

import java.util.Locale;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import lombok.SneakyThrows;
import training.speedreading.DataBridge;
import training.speedreading.ext.LocaleListCell;
import training.speedreading.manager.SettingManager;
import training.speedreading.util.GameUtil;

/**
 * Main.fxml controller.
 * @author Alexey Chalov
 */
public class MainController {

    @FXML
    private ComboBox<Locale> languageCombobox;

    private static final SettingManager SETTING_MANAGER = SettingManager.instance();
    private static final DataBridge DATA_BRIDGE = DataBridge.instance();

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        languageCombobox.setItems(observableArrayList(GameUtil.LOCALE_RU, GameUtil.LOCALE_UK));
        languageCombobox.setValue(SETTING_MANAGER.getLocale());
        languageCombobox.setButtonCell(new LocaleListCell());
        languageCombobox.setCellFactory((p) -> new LocaleListCell());
    }

    /**
     * New game navigation.
     */
    @FXML
    @SneakyThrows
    private void newGame() {
        DATA_BRIDGE.getApplication().getStage().getScene().setRoot(
            new FXMLLoader(getClassPathResourceUrl("view/NewGame.fxml"), DATA_BRIDGE.getResourceBundle()).load()
        );
    }

    /**
     * Change locale event handler.
     */
    @FXML
    private void changeLocale() {
        Locale locale = languageCombobox.getValue();
        if (!SETTING_MANAGER.getLocale().equals(locale)) {
            SETTING_MANAGER.saveLocale(locale);
            DATA_BRIDGE.getApplication().buildStage();
        }
    }

    /**
     * Exit event handler.
     */
    @FXML
    private void exit() {
        System.exit(0);
    }
}
