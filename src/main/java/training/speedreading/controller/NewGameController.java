package training.speedreading.controller;

import static javafx.collections.FXCollections.observableArrayList;
import static training.speedreading.manager.SettingManager.MAX_GAME_SIZE;
import static training.speedreading.manager.SettingManager.MIN_GAME_SIZE;
import static training.speedreading.util.GameUtil.getClassPathResourceUrl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import lombok.SneakyThrows;
import training.speedreading.DataBridge;
import training.speedreading.ext.GameSizeListCell;
import training.speedreading.ext.SpeedDynamicsListCell;
import training.speedreading.manager.SettingManager;
import training.speedreading.model.GameSize;
import training.speedreading.model.SpeedDynamics;

/**
 * NewGame.fxml controller.
 * @author Alexey Chalov
 */
public class NewGameController {

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    @FXML
    private ComboBox<GameSize> gameSizeCombobox;
    @FXML
    private ComboBox<SpeedDynamics> speedDynamicsCombobox;
    @FXML
    private Slider speedSlider;
    @FXML
    private Label speedValueLabel;

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        gameSizeCombobox.setValue(SETTING_MANAGER.getGameSize());
        List<GameSize> values = IntStream.range(MIN_GAME_SIZE, MAX_GAME_SIZE + 1)
                                       .mapToObj((i) -> new GameSize(i, i))
                                       .collect(Collectors.toList());
        gameSizeCombobox.setItems(observableArrayList(values));
        gameSizeCombobox.setButtonCell(new GameSizeListCell());
        gameSizeCombobox.setCellFactory(p -> new GameSizeListCell());

        speedDynamicsCombobox.setValue(SETTING_MANAGER.getSpeedDynamics());
        speedDynamicsCombobox.setItems(observableArrayList(SpeedDynamics.values()));
        speedDynamicsCombobox.setButtonCell(new SpeedDynamicsListCell());
        speedDynamicsCombobox.setCellFactory(p -> new SpeedDynamicsListCell());

        int speed = SETTING_MANAGER.getSpeed();
        speedSlider.setValue(speed);
        speedValueLabel.setText(String.valueOf(speed));
        speedSlider.valueProperty().addListener(
            (s, o, n) -> speedValueLabel.setText(String.valueOf(adjustSliderValue(n)))
        );
    }

    /**
     * Play game navigation.
     */
    @FXML
    @SneakyThrows
    private void startGame() {
        SETTING_MANAGER.saveSettings(
            gameSizeCombobox.getValue(), speedDynamicsCombobox.getValue(), adjustSliderValue(speedSlider.getValue())
        );
        DATA_BRIDGE.getApplication().getStage().getScene().setRoot(
            new FXMLLoader(getClassPathResourceUrl("view/Game.fxml"), DATA_BRIDGE.getResourceBundle()).load()
        );
    }

    /**
     * Main menu navigation.
     */
    @FXML
    private void mainMenu() {
        DATA_BRIDGE.getApplication().buildStage();
    }

    /**
     * Returns slider value with regard to tick length.
     * @param n initial slider value
     * @return adjusted slider value
     */
    private int adjustSliderValue(Number n) {
        return (int) (n.doubleValue() - (n.doubleValue() % speedSlider.getBlockIncrement()));
    }
}
