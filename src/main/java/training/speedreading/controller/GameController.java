package training.speedreading.controller;

import static training.speedreading.util.GameUtil.formatTime;
import static training.speedreading.util.GameUtil.getClassPathResourceUrl;
import static training.speedreading.manager.SettingManager.MIN_GAME_SIZE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import training.speedreading.DataBridge;
import training.speedreading.manager.SettingManager;
import training.speedreading.model.GameSize;
import training.speedreading.model.SpeedDynamics;
import training.speedreading.util.GameUtil;

/**
 * Game.fxml controller.
 * @author Alexey Chalov
 */
public class GameController {

    private static final DataBridge DATA_BRIDGE = DataBridge.instance();
    private static final SettingManager SETTING_MANAGER = SettingManager.instance();

    //TODO: move in settings
    private static final int MAX_GAME_TIME = 3 * 60 * 1000;
    private static final int MAX_ERROR_COUNT = 3;

    @FXML
    private Label currentSpeedLabel;
    @FXML
    private Label timeLeftLabel;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label finalScoreLabel;
    @FXML
    private Label maxSpeedLabel;
    @FXML
    private HBox errorsBox;
    @FXML
    private VBox containerPane;
    @FXML
    private GridPane gamePane;
    @FXML
    private GridPane selectWordPane;
    @FXML
    private VBox gameResultPane;

    private GameSize gameSize;

    private Timeline timerTimeline;
    private Timeline wordsTimeline;

    private long startTime;
    private int currentSpeed;
    private int maxSpeed;
    private int currentScore;
    private int errorCount;
    private List<String> wordList;
    private Iterator<String> wordListIterator;

    private Map<GameSize, Integer> gameSizeToFontSize =
        IntStream.range(SettingManager.MIN_GAME_SIZE, SettingManager.MAX_GAME_SIZE + 1).boxed()
                 .collect(Collectors.toMap(
                     k -> new GameSize(k, k),
                     k -> 22 - (k - SettingManager.MIN_GAME_SIZE) * 2,
                     (o1, o2) -> o1,
                     HashMap::new
                 ));

    /**
     * Initializer method.
     */
    @FXML
    private void initialize() {
        gameSize = SETTING_MANAGER.getGameSize();
        startTime = System.currentTimeMillis();

        timerTimeline = new Timeline(new KeyFrame(Duration.seconds(1), e -> setupTimerLabel()));
        timerTimeline.setCycleCount(Animation.INDEFINITE);

        //TODO: may need to move somewhere else, cause replay button click will use latest current speed, not from settings
        //TODO: use fxml include on game pane
        currentSpeed = SETTING_MANAGER.getSpeed();

        setupCurrentSpeedLabel();
        setupErrorsBox();
        setupTimerLabel();
        setupScoreLabel();
        setupGamePane();
        initGamePane();
        timerTimeline.playFromStart();
    }

    /**
     * Main menu navigation.
     */
    @FXML
    private void mainMenu() {
        timerTimeline.stop();
        wordsTimeline.stop();
        DATA_BRIDGE.getApplication().buildStage();
    }

    /**
     * Sets up errors box.
     */
    private void setupErrorsBox() {
        errorsBox.getChildren().clear();
        IntStream.range(0, MAX_ERROR_COUNT).forEach(i -> {
            HBox container = new HBox();
            container.setAlignment(Pos.CENTER);
            container.setMinWidth(19);
            ImageView imageView = new ImageView(new Image(
                getClassPathResourceUrl(i < errorCount ? "image/error.png" : "image/error_blank.png").toExternalForm()
            ));
            container.getChildren().add(imageView);
            errorsBox.getChildren().add(container);
        });
    }

    /**
     * Sets up game pane with constraints.
     */
    private void setupGamePane() {
        IntStream.range(0, gameSize.getWidth()).forEach(h -> {
            ColumnConstraints cc = new ColumnConstraints();
            cc.setHalignment(HPos.CENTER);
            gamePane.getColumnConstraints().add(cc);
        });
        IntStream.range(0, gameSize.getHeight()).forEach(v -> {
            RowConstraints rc = new RowConstraints();
            rc.setValignment(VPos.CENTER);
            gamePane.getRowConstraints().add(rc);
        });
    }

    /**
     * Initializes game pane.
     */
    private void initGamePane() {
        maxSpeed = currentSpeed;
        if (!containerPane.getChildren().contains(gamePane)) {
            containerPane.getChildren().add(gamePane);
        }
        containerPane.getChildren().remove(selectWordPane);
        containerPane.getChildren().remove(gameResultPane);
        gamePane.getChildren().clear();

        int size = getGameAreaSize();
        GameSize gameSize = SETTING_MANAGER.getGameSize();
        int cellSize = Math.min(size / gameSize.getWidth(), size / gameSize.getHeight());

        wordList = GameUtil.newWordList(SETTING_MANAGER.getLocale(), gameSize.getHeight() * gameSize.getWidth());

        wordListIterator = wordList.iterator();

        wordsTimeline = new Timeline(new KeyFrame(Duration.millis(60000 / currentSpeed), e -> setupNextWord()));
        wordsTimeline.setCycleCount(wordList.size() + 1);
        wordsTimeline.setOnFinished(e -> initWordSelectionPane());
        wordsTimeline.playFromStart();

        IntStream.range(0, gameSize.getHeight()).forEach(row -> {
            IntStream.range(0, gameSize.getWidth()).forEach(col -> {
                HBox cell = createCell(cellSize);
                cell.getStyleClass().add("game-grid-cell");
                gamePane.add(cell, col, row);
            });
        });
    }

    /**
     * Removes game pane and shows word selection one on time line completion.
     */
    private void initWordSelectionPane() {
        containerPane.getChildren().remove(gamePane);
        containerPane.getChildren().add(selectWordPane);
        List<String> selectWords = new ArrayList<>(wordList.subList(wordList.size() - 6, wordList.size()));
        Collections.shuffle(selectWords);

        Iterator<String> selectedWordsIterator = selectWords.iterator();
        selectWordPane.getChildren().stream().forEach(c -> {
            HBox cell = (HBox) c;
            String word = selectedWordsIterator.next();
            cell.setOnMouseClicked(e -> selectWord(cell, word));
            ((Label) cell.getChildren().get(0)).setText(word);
        });
    }

    /**
     * Shows game result pane when game is over.
     */
    private void initGameResultPane() {
        containerPane.getChildren().remove(selectWordPane);
        containerPane.getChildren().add(gameResultPane);
        timerTimeline.stop();
        finalScoreLabel.setText(String.format(DATA_BRIDGE.getResourceBundle().getString("game.finalScore"), currentScore));
        maxSpeedLabel.setText(String.format(DATA_BRIDGE.getResourceBundle().getString("game.maxSpeed"), maxSpeed));
    }

    /**
     * Latest word selection handler method.
     * @param cell {@link HBox} cell
     * @param word word to use as label
     */
    private void selectWord(HBox cell, String word) {
        //TODO: sounds on word selection, on game complete
        //TODO: replay, sound buttons
        //TODO: выровнять строки на панели результатов игры
        String imagePath;
        //TODO: hardcoded values below, take from somewhere else
        int minSpeed = 50;
        int maxSpeed = 2000;
        int increment = 50;
        if (!word.equals(wordList.get(wordList.size() - 1))) {
            if (SETTING_MANAGER.getSpeedDynamics() == SpeedDynamics.ADJUSTING && currentSpeed - increment >= minSpeed) {
                currentSpeed -= increment;
                setupCurrentSpeedLabel();
            }
            errorCount++;
            setupErrorsBox();
            imagePath = "image/wrong.png";
        } else {
            currentScore += Math.pow(2, SETTING_MANAGER.getGameSize().getHeight() - MIN_GAME_SIZE) * currentSpeed / increment;
            setupScoreLabel();
            if (SETTING_MANAGER.getSpeedDynamics() == SpeedDynamics.ADJUSTING && currentSpeed + increment <= maxSpeed) {
                currentSpeed += increment;
                setupCurrentSpeedLabel();
                maxSpeed = currentSpeed;
            }
            imagePath = "image/correct.png";
        }
        HBox.setMargin(cell.getChildren().get(0), new Insets(0, 0, 0, 29));
        ImageView imageView = new ImageView(new Image(getClassPathResourceUrl(imagePath).toExternalForm()));
        cell.getChildren().add(imageView);
        HBox.setMargin(imageView, new Insets(0, 0, 0, 5));
        selectWordPane.getChildren().forEach(c -> {
            c.setOnMouseClicked(null);
        });
        Timeline delayTimeline = new Timeline();
        delayTimeline = new Timeline(new KeyFrame(Duration.millis(300), e -> nextRound()));
        delayTimeline.setCycleCount(1);
        delayTimeline.setOnFinished(e -> {
            HBox.setMargin(cell.getChildren().get(0), new Insets(0, 0, 0, 0));
            cell.getChildren().remove(imageView);
        });
        delayTimeline.playFromStart();
    }

    /**
     * Shows next round words or game over screen, of time is over or errors limit reached.
     */
    private void nextRound() {
        if (errorCount >= MAX_ERROR_COUNT || System.currentTimeMillis() - startTime > MAX_GAME_TIME) {
            initGameResultPane();
        } else {
            initGamePane();
        }
    }

    /**
     * Creates game cell.
     * @param size cell size
     * @return created cell
     */
    private HBox createCell(int size) {
        HBox cell = new HBox();
        cell.setMinWidth(size);
        cell.setMinHeight(size / 2);
        cell.setAlignment(Pos.BOTTOM_CENTER);
        Label label = new Label();
        switch (gameSize.getWidth()) {
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            default:
                break;
        }

        label.setStyle("-fx-font-size: " + gameSizeToFontSize.get(gameSize) + " px;");
        cell.getChildren().add(label);
        return cell;
    }

    /**
     * Sets up new word for next grid cell, clears previous.
     */
    private void setupNextWord() {
        List<Label> labels = gamePane.getChildren().stream()
                                     .map(c -> (Label) ((HBox) c).getChildren().get(0))
                                     .collect(Collectors.toList());
        for (int i = 0; i < labels.size(); i++) {
            Label label = labels.get(i);
            if (label.getText() != null && !label.getText().trim().isEmpty()) {
                if (!wordListIterator.hasNext()) {
                    /* last tick */
                    label.setText(null);
                    return;
                }
                label.setText(new String());
                if (i < labels.size() - 1) {
                    /* set next */
                    labels.get(i + 1).setText(wordListIterator.next());
                } else {
                    /* next cycle: start showing words from first cell */
                    labels.get(0).setText(wordListIterator.next());
                }
                return;
            }
        }
        labels.get(0).setText(wordListIterator.next());
    }

    /**
     * Sets up timer label.
     */
    private void setupTimerLabel() {
        timeLeftLabel.setText(String.format(
            DATA_BRIDGE.getResourceBundle().getString("game.timer"),
            formatTime(Math.max(
                MAX_GAME_TIME - Math.floor((System.currentTimeMillis() - startTime) / 1000) * 1000,
                0
            ))
        ));
    }

    /**
     * Sets up score label.
     */
    private void setupScoreLabel() {
        scoreLabel.setText(String.format(DATA_BRIDGE.getResourceBundle().getString("game.score"), currentScore));
    }

    /**
     * Sets up current speed label.
     */
    private void setupCurrentSpeedLabel() {
        currentSpeedLabel.setText(String.format(DATA_BRIDGE.getResourceBundle().getString("game.currentSpeed"), currentSpeed));
    }

    /**
     * Returns game area size.
     * @return game area size
     */
    private int getGameAreaSize() {
        return (int) (((Pane) gamePane.getParent()).getPrefHeight() * 0.95);
    }
}
