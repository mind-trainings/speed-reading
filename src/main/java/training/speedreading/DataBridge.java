package training.speedreading;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import lombok.Getter;
import training.speedreading.manager.SettingManager;

/**
 * Data bridge for controllers.
 * @author Alexey Chalov
 */
public final class DataBridge {

    @Getter
    private SpeedReading application;
    private static DataBridge instance = new DataBridge();

    /**
     * Constructor.
     */
    private DataBridge() {
    }

    /**
     * Returns instance of data bridge.
     * @return {@link DataBridge} instance
     */
    public static DataBridge instance() {
        return instance;
    }

    /**
     * Returns application level resource bundle.
     * @return {@link ResourceBundle} instance
     */
    public ResourceBundle getResourceBundle() {
        return ResourceBundle.getBundle("bundle/strings", SettingManager.instance().getLocale(), new Control() {
            public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                                            throws IllegalAccessException, InstantiationException, IOException {
                String bundleName = toBundleName(baseName, locale);
                String resourceName = toResourceName(bundleName, "properties");
                ResourceBundle bundle = null;
                InputStream stream = null;
                if (reload) {
                    URL url = loader.getResource(resourceName);
                    if (url != null) {
                        URLConnection connection = url.openConnection();
                        if (connection != null) {
                            connection.setUseCaches(false);
                            stream = connection.getInputStream();
                        }
                    }
                } else {
                    stream = loader.getResourceAsStream(resourceName);
                }
                if (stream != null) {
                    try {
                        bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
                    } finally {
                        stream.close();
                    }
                }
                return bundle;
            }
        });
    }

    /**
     * Sets application main class instance.
     * @param application {@link SpeedReading} instance
     */
    void setApplication(SpeedReading application) {
        this.application = application;
    }
}
