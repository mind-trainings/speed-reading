package training.speedreading;

import static training.speedreading.util.GameUtil.getClassPathResourceUrl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;

/**
 * Starts application.
 * @author Alexey Chalov
 */
public class SpeedReading extends Application {

    private Stage stage;
    private static final DataBridge DATA_BRIDGE = DataBridge.instance();

    /**
     * Starts application.
     * @param args program arguments
     */
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClassPathResourceUrl("image/logo.png").toExternalForm()));

        DATA_BRIDGE.setApplication(this);

        buildStage();
    }

    /**
     * Builds stage.
     */
    @SneakyThrows
    public void buildStage() {
        Scene scene = new Scene(new FXMLLoader(getClassPathResourceUrl("view/Main.fxml"), DATA_BRIDGE.getResourceBundle()).load());
        scene.getStylesheets().add(getClassPathResourceUrl("style/application.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle(DATA_BRIDGE.getResourceBundle().getString("title"));
        stage.show();
    }

    /**
     * Returns current {@link Stage} instance.
     * @return {@link Stage} instance
     */
    public Stage getStage() {
        return stage;
    }
}
