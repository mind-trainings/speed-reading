**Игра-тренажер скорочтения**

***
Technologies / Стек технологий: Java SE 11+, JavaFX, Lombok, Maven

***
How to run / Запуск игры:

    Maven: mvn javafx:run

    Java SE 11+: main class training.speedreading.SpeedReading,
                 VM arguments --module-path <JAVAFX_SDK_PATH>/lib  --add-modules javafx.controls,javafx.fxml,javafx.media

***
TODO list / Список доработок:

    //TODO: single instance не работает
    //TODO: compress images
    //TODO: use DB instead of preferences
    //TODO: achievements screen
    //TODO: extract common module
    //TODO: extract game pane and result page from game.fxml
    //TODO: single instance
    //TODO: keep dictionary on DB
